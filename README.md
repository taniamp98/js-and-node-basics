# Javascript + Node.js fundamentals

1. **GET**: Vrem sa printam toti crocodilii din baza de date. In cazul in care este specificat cu ajutorul unui [query param](https://guides.emberjs.com/release/routing/query-params/) statusul activ true/false vom afisa doar crocodilii activi/inactivi

```
app.get("/crocodili", (req, res) => {
  try {
    let select = "";
    if (req.query.activ) {
      select = `SELECT * FROM Crocodili WHERE activ = '${req.query.activ}'`;
    } else {
      select = "SELECT * FROM Crocodili";
    }
    connection.query(select, (err, result) => {
      if (err) throw err;
      res.status(200).send(result);
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});
```

- Tipul requestului este `get` deoarece dorim doar sa afisam ceva existent in baza de date.
- Avem o conditie if pentru a verifica existenta unui query param numit activ: `req.query.param`.
- Daca exista, instructiunea sql va intoarce toti crocodilii unde statusul de activ coincide cu cel indicat la request
- Daca nu exista intoarcem toti crocodilii
- Trimitem inapoi un raspuns: 200 daca totul a decurs ok, 500 daca a picat serverul

2. **DELETE**: Stergem un crocodil in functie de id-ul din baza de date. De data aceasta id-ul este specificat printr-un [params](https://sailsjs.com/documentation/reference/request-req/req-params) simplu.

```
app.delete("/crocodili/:id", (req, res) => {
  try {
    const sqlDelete = `DELETE FROM Crocodili WHERE id = '${req.params.id}'`;
    connection.query(sqlDelete, (err) => {
      if (err) throw err;
      res.status(200).send({ message: "Crocodil disparut" });
    });
  } catch {
    console.log("Server error");
    res.status(500).send(err);
  }
});
```

- Tipul requestului este delete deoarece vrem sa stergem ceva din baza de date
- Apelam instructiunea sql ce sterge din baza de date in functie de id-ul pe care il luam din `req.params.id`
- Intoarcem un status: 200 OK sau 500 Server Error

# Tema

## Javascript

1. Creeaza un obiect `rezervari` cu urmatoarele proprietati: `numarRezervariTotale`, `numePersoane` (lista persoanelor ce au inregistrat o rezervare), `nrPersoaneLaMasa` (lista cu numarul de persoane pentru fiecare rezervare).

- Afiseaza la consola obiectul cu urmatoarele valori pentru proprietati

```
numarRezervariTotale = 3
numePersoane = ["Croco", "Tania", "Adrian"]
nrPersoaneLaMasa = [6, 2, 3]
```

- Adauga o noua rezervare pe numele "Mihnea" de 10 persoane. `Hint:` ce se intampla cu numarul de rezervari Totale?

2. Creeaza o functie ce sa:

- calculeze numarul total de persoane prezente in restaurant
- afiseaza la consola un vector cu fiecare persoana si numarul sau de persoane rezervat. Ex:

```
["Croco-6", "Tania-2", "Adrian-3"]
```

## Node.js

Continuand scriptul din fisierul server.js creati doua noi requesturi:

1. **GET by id**: Intoarceti drept raspuns doar crocodilul cu id-ul din baza de date ca cel din request.

```
Ruta Postman: GET: localhost:8080/crocodili/:id
```

2. **PUT by id**: Modificati campurile unui anumit recrut. `Hint:` nu uitati de body!

```
Ruta Postman: PUT: localhost:8080/crocodili/:id
```
