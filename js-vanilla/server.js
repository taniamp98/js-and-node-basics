//afisarea la consola
console.log("Hello crocos!");

//declarari de variabile
let var1 = 3;
var1 = "tania";
console.log(var1);

const var2 = 6;
//var2 = 9;
console.log(var2);

var var3 = "crocos";

//declarari de functi
function greeting() {
  console.log("Hello world!");
}

function greeting(name) {
  console.log("heloo " + name);
}

function greeting(name, age) {
  console.log("heloo " + name);
}

greeting();
greeting("Tania");
greeting("Croco");

let sum = function (a, b) {
  return a + b;
};

console.log(sum(2, 3));
let x = sum(4, 4);
console.log(x);

let arrowFunction = (n) => {
  return (n * (n + 1)) / 2;
};

console.log(arrowFunction(10));

//vectori si metode principale
let array = [12, 3, "Banana", 9.0];

console.log(array[2]);

array.push(10);
console.log(array);
array.pop();
console.log(array);
array.splice(2, 0, "Ana");
console.log(array);

let propozitie = "Imi place sa codez.";
let cuvinte = propozitie.split(" ");
console.log(cuvinte);
let fructe = "banana,mar,para";
let fructeVector = fructe.split(",");
console.log(fructeVector);

//structuri conditionale
if (2 == 1) {
  console.log("egale");
} else {
  console.log("nu sunt egale");
}

//structuri repetitive
for (let i = 0; i < fructeVector.length; i++) {
  console.log(fructeVector[i]);
}

//obiecte / json
let obiect = {
  nume: "Tania",
  varsta: 20,
  adresa: ["Bucuresti", "Bacau"],
  data: {
    zi: 25,
    luna: 11,
    an: 2021,
  },
};

obiect.nume = "Croco";
console.log(obiect);

obiect.data.luna = 12;
console.log(obiect);
